PyJapc
======

``PyJapc`` is a Python library to interact an accelerator the control system via the
"Java API for Parameter Control" (JAPC).

Find the full documentation at: https://acc-py.web.cern.ch/gitlab/scripting-tools/pyjapc

With ``PyJapc`` you can:

* GET and SET parameters asynchronously.

* GET several parameters from the same time-instance (JAPC ParameterGroups).

* SET one, several or all fields of a device property in one operation.

* SUBSCRIBE to parameters by registering your own python callback functions.

* The most common data types are automatically converted to (and from) python native types.

* 1D and 2D arrays, are converted to (and from) numpy arrays.

* If a datatype is not known, the JAPC ParameterValue Object is returned, which can be manipulated in Python.

* Data filters are supported and can be specified as Python dict.

* RBAC and INCA authentication is supported.

* Under the hood PyJapc makes use of JPype to call JAPC functions directly from Python.
  The idea is to provide a simplified interface to FESA, which you can use without knowing anything about the underlying JAPC API.
  If more complex functionality is required, it is possible to manually call the relevant JAPC functions from Python.

Example::

    import pyjapc

    # Create a PyJapc instance with selector SCT.USER.ALL
    # INCA is automatically configured based on the timing domain you specify here
    japc = pyjapc.PyJapc("SCT.USER.ALL")

    # You can change the selector later, but note that the INCA server cannot be changed
    japc.setSelector("SCT.USER.SETUP")

    # (optional) RBAC login
    japc.rbacLogin("user_name")

    # Get information about a parameter
    print(japc.getParamInfo("CB.BHB1100/Acquisition#currentAverage"))

    # Get the value of a parameter
    print(japc.getParam("CB.BHB1100/Acquisition#currentAverage"))

    # Close the RBAC session
    japc.rbacLogout()