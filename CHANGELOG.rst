Changelog
=========

**2015-03-21**

* Initial version, `getParam()` works for values and arrays.

**2015-03-22**

* Added `getParamInfo()` and `setParam()` functions.

**2015-03-23**

* Added MAP support for GET (if an incomplete FESA path is given, all relevant
  FESA parameters are returned).

**2015-03-24**

* Supports now numpy arrays. GET and SET of 1D and 2D arrays should be possible.

**2015-03-25**

* Fixed log4j.
* Added INCA login.

**2015-03-26**

* Made PyJapc compatible with Python 3.

**2015-03-27**

* Added array dimension check in Python.
* Added experimental subscription support.
* Added support for DiscreteFunction values.

**2015-03-29**

* Added MAP support for SET. Depending on input values, a FULL or PARTIAL SET
  is carried out.
* Subscribing should now work fine on Python 3.

**2015-04-08**

* Added `clearSubscriptions()`, added (some) Windows support,

**2015-04-12**

* In `setSelector()` a filter can now be specified as Python dict()

**2015-05-05**

* Made compatible with JPype v.0.6.0.

**2015-05-07**

* Improved speed of array type conversion (Java to Python) by a factor of
  500 (!).
* `JAR_PATHS` is now a list of paths.
* Might have broken compatibility with Windows...

**2015-05-11**

* Tried to make the array dimension check (Numpy vs. Japc) more robust
  `filter` + `selector` + `onChange` setting can now be temporarily overridden
  in the `getParam()`, `getParams()` and `setParam()` methods.
* When INCA is disabled (`incaAcceleratorName=None`) I will fall back on the
  (outdated) japc-ext-dirservice to get ValueDescriptors.

**2015-06-17**

* Added workarounds to support getting and setting empty arrays.

**2015-06-29**

* Added graphical login dialog. Useful for python-consoles which would
  otherwise echo and reveal the password (like the Eclipse IDE). Use it like
  this: `japc.rbacLogin(loginDialog=True)` (thanks to H. Timko for the
  TkInter code).

**2015-07-09**

* Added the `getParams()` method, which allows to GET a list of parameters from
  the _same_ instance in time. This command also returns the FESA timestamp
  for each parameter acquisition. Functionality relies on JAPC
  `Parameter-Groups <https://confluence.cern.ch/display/JAPC/Parameter+Groups>`__.

**2015-08-22**

* Added a special case in `getParam()` for primitive values, so they are not
  handled as 2D arrays any more.

**2015-08-24**

* pyjapc2.py upgraded JAPC to version 3.2.2.
* I'm using my own collection of .jars now.

**2015-10-14**

* pyjapc.py uses the new jars now by default.
* Added support for getting `DiscreteFunction` and `DiscreteFunctionList`
  objects (Trim).
* Also added super-experimental support for SETTING them with a 2D array / a
  list of 2D arrays respectively.
* Furthermore some cleanup behind the scenes of the SET and type-conversion
  functions.

**2016-01-11**

* In `_convertPyToSimpleVal()` I'm doing now always an explicit type
  conversion with a lookup table to find the right Java type.
* Made pyjapc a proper Python package.
* Switched to the latest .jar files (japc.jar v4.0.0) and JVM (1.8).

**2016-01-12**

* Reworked type conversion. See docu of `setParam()` on how to define a
  specific Java variable type.

**2016-01-14**

* Fixed `rbacLogin(loginDialog=True)` when using PyJapc as a package.
* Default login dialog to current username if none is provided.
* Cleanup of login dialog layout to better match Java version.

**2016-01-15**

* New API documentation and examples using Sphinx.

**2016-01-18**

* Now use logger for all output from PyJapc. Most messages are emitted with a
  level of INFO, so to view them it is necessary to call the logger method:
  `logger.basicConfig(level=logger.INFO)` to view them.

**2016-03-14**

* Added the getHeader=True argument to getParam() and subscribeParam(). The
  header is a dict() containing `acqStamp`, `cycleStamp`, `isImmediateUpdate`
  and `isFirstUpdate`. Timestamps are `datetime` objects, by default in UTC
  time. Timezone can be changed by constructor `PyJapc( timeZone='local' )`
  When getParam() or subscribeParam() is called with a list of parameter
  names, a JAPC `ParameterGroup` is automatically created, and JAPC will try
  to synchronize updates.

**2016-03-19**

* Added optional `password` parameter to `rbacLogin()` although its use is
  discouraged.

**2016-03-22**

* Improved `rbacLogout()` to only perform the logout if there is a token.
* Updated `rbacLogin()` to raise an exception if the user cancels the login
  dialog. Previously the function would return and there would be no way to
  tell that the login had been cancelled.
* Cleaned up the source code to meet PEP8 standards.

**2018-11-30**

* Initial adaptation to LS2 JAPC changes.

**2020-01-17**

* A warning is raised and the rbac_dialog functionality is disabled if
  Tkinter is not available from the Python in which PyJapc is installed.

**2020-01-18**

* Please refer to the GitLab releases page for details of changes to PyJapc.
  https://gitlab.cern.ch/scripting-tools/pyjapc/-/releases
