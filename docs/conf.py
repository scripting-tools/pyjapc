import pyjapc
import datetime


extensions = [
    'acc_py_sphinx.theme',
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.napoleon',
    'sphinx.ext.autosummary',
    'sphinx.ext.autodoc.typehints',
]


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
source_suffix = ['.rst', '.md']

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = 'PyJapc'
copyright = f'2015 - {datetime.datetime.now().year}, CERN'
author = 'CERN'

# The full version, including alpha/beta/rc tags.
release = pyjapc.__version__
# The short X.Y version.
version = '.'.join(release.split('.')[:3])

language = 'en'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = ['build']

# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'acc_py'


autoclass_content = 'both'

autodoc_typehints = "description"
