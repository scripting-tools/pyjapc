
Example 1: A demonstration of some PyJapc commands
==================================================

Author: Michael Betz, CERN BE-BI

Setup
-----

.. code:: python

    import numpy as np
    import pprint

For this example we will initialize PyJapc in "safe mode" so all sets
are only simulated (i.e. are not written to the device).

.. code:: python

    import pyjapc
    
    japc = pyjapc.PyJapc(noSet=True)

Do a simple GET
---------------

Our test subject is some magnet in CTF3, so we need to set the selector
to ``SCT.USER.ALL``.

.. code:: python

    japc.setSelector("SCT.USER.ALL")
    print(japc.getParam("CB.BHB1100/Acquisition#currentAverage"))
    print(japc.getParam("CB.BHB1100/Acquisition#current_unit"))


.. parsed-literal::

    0.0
    A


Now we will GET a 1D array
--------------------------

Our victim is the *bunch - selector* value of the LHC Schottky monitor.
A zero value means that this particular bucket is gated out in the
acquisition.

The return value is a numpy array, ready for calculations or indexing.
Note that 2D arrays are also supported.

.. code:: python

    japc.setSelector("LHC.USER.ALL")
    arrData = japc.getParam("LHC.BQS.SCTL/BunchSelector#BunchSel1Slots")
    print(arrData)
    print(arrData.shape)
    print(type(arrData))


.. parsed-literal::

    [1 1 1 ..., 0 0 0]
    (3564,)
    <class 'numpy.ndarray'>


Get a MAP of parameters
-----------------------

When the exact parameter is not specified with the "**#**\ " tag, the
whole map of parameters is returned. The return value in this case is a
Python dictionary.

This is actually very useful to browse FESA classes interactively.

.. code:: python

    japc.setSelector("SCT.USER.ALL")
    mapOfValues = japc.getParam("CB.BHB1100/Acquisition")
    pprint.pprint(mapOfValues)


.. parsed-literal::

    {'acqStamps': array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0]),
     'current': array([ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
            0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
            0.,  0.,  0.,  0.,  0.,  0.]),
     'currentAverage': 0.0,
     'current_status': 0,
     'current_unit': 'A'}


Note that the previous result was just a Python dictionary; we can
access individual values like this

.. code:: python

    print(mapOfValues["currentAverage"])


.. parsed-literal::

    0.0


Note that this is just a local variable. Overwriting values is possible
but will not have any effect on the FESA parameter.

.. code:: python

    mapOfValues["currentAverage"] = 42.0
    print(mapOfValues["currentAverage"])
    mapOfValues = japc.getParam("CB.BHB1100/Acquisition")
    print(mapOfValues["currentAverage"])


.. parsed-literal::

    42.0
    0.0


JAPC can provide much info about a parameter
--------------------------------------------

Note that this is a **string** returned from JAPC.

.. code:: python

    print(japc.getParamInfo("CB.BHB1100/Acquisition#currentAverage"))


.. parsed-literal::

    Parameter descriptor for CB.BHB1100/Acquisition
    Type               [Map]
    Description=       [Reads current information (raw data, average,.]
    isCycleDependent=  [true]
    isReadable=        [true]
    isMonitorable=     [true]
    isTransactional=   [false]
    isWritable=        [false]
    isValueMutable=    [false]
    Extra charact.=   
      DATA_SOURCE= [CCDB]
      IS_CCDB_DESCRIPTOR= [true]
      IS_VALID= [true]
      PARAMETER_URL_GET= [rda3://rda3/CB.BHB1100/Acquisition]
      PARAMETER_URL_SET= [rda3://rda3/CB.BHB1100/Acquisition]
    Fields= [
      acqStamp --> Parameter descriptor for CB.BHB1100/Acquisition#acqStamp
        Type               [Simple]
        Description=       [-]
        isCycleDependent=  [false]
        isReadable=        [true]
        isMonitorable=     [true]
        isTransactional=   [false]
        isWritable=        [false]
        isValueMutable=    [false]
        Extra charact.=   
          DATA_SOURCE= [CCDB]
          IS_CCDB_DESCRIPTOR= [true]
          IS_RESTORABLE= [false]
          IS_SAVABLE= [false]
          IS_STATUS_PROPERTY= [false]
          IS_VALID= [true]
          PARAMETER_URL_GET= [rda3://rda3/CB.BHB1100/Acquisition#acqStamp]
          PARAMETER_URL_SET= [rda3://rda3/CB.BHB1100/Acquisition#acqStamp]
        
      current --> Parameter descriptor for CB.BHB1100/Acquisition#current
        Type               [Simple]
        Description=       [-]
        isCycleDependent=  [false]
        isReadable=        [true]
        isMonitorable=     [true]
        isTransactional=   [false]
        isWritable=        [false]
        isValueMutable=    [false]
        Extra charact.=   
          DATA_SOURCE= [CCDB]
          IS_CCDB_DESCRIPTOR= [true]
          IS_RESTORABLE= [false]
          IS_SAVABLE= [false]
          IS_STATUS_PROPERTY= [false]
          IS_VALID= [true]
          PARAMETER_URL_GET= [rda3://rda3/CB.BHB1100/Acquisition#current]
          PARAMETER_URL_SET= [rda3://rda3/CB.BHB1100/Acquisition#current]
        
      updateFlags --> Parameter descriptor for CB.BHB1100/Acquisition#updateFlags
        Type               [Simple]
        Description=       [-]
        isCycleDependent=  [false]
        isReadable=        [true]
        isMonitorable=     [true]
        isTransactional=   [false]
        isWritable=        [false]
        isValueMutable=    [false]
        Extra charact.=   
          DATA_SOURCE= [CCDB]
          IS_CCDB_DESCRIPTOR= [true]
          IS_RESTORABLE= [false]
          IS_SAVABLE= [false]
          IS_STATUS_PROPERTY= [false]
          IS_VALID= [true]
          PARAMETER_URL_GET= [rda3://rda3/CB.BHB1100/Acquisition#updateFlags]
          PARAMETER_URL_SET= [rda3://rda3/CB.BHB1100/Acquisition#updateFlags]
        
      current_unit --> Parameter descriptor for CB.BHB1100/Acquisition#current_unit
        Type               [Simple]
        Description=       [-]
        isCycleDependent=  [false]
        isReadable=        [true]
        isMonitorable=     [true]
        isTransactional=   [false]
        isWritable=        [false]
        isValueMutable=    [false]
        Extra charact.=   
          DATA_SOURCE= [CCDB]
          IS_CCDB_DESCRIPTOR= [true]
          IS_RESTORABLE= [false]
          IS_SAVABLE= [false]
          IS_STATUS_PROPERTY= [false]
          IS_VALID= [true]
          PARAMETER_URL_GET= [rda3://rda3/CB.BHB1100/Acquisition#current_unit]
          PARAMETER_URL_SET= [rda3://rda3/CB.BHB1100/Acquisition#current_unit]
        
      cycleStamp --> Parameter descriptor for CB.BHB1100/Acquisition#cycleStamp
        Type               [Simple]
        Description=       [-]
        isCycleDependent=  [false]
        isReadable=        [true]
        isMonitorable=     [true]
        isTransactional=   [false]
        isWritable=        [false]
        isValueMutable=    [false]
        Extra charact.=   
          DATA_SOURCE= [CCDB]
          IS_CCDB_DESCRIPTOR= [true]
          IS_RESTORABLE= [false]
          IS_SAVABLE= [false]
          IS_STATUS_PROPERTY= [false]
          IS_VALID= [true]
          PARAMETER_URL_GET= [rda3://rda3/CB.BHB1100/Acquisition#cycleStamp]
          PARAMETER_URL_SET= [rda3://rda3/CB.BHB1100/Acquisition#cycleStamp]
        
      current_status --> Parameter descriptor for CB.BHB1100/Acquisition#current_status
        Type               [Simple]
        Description=       [-]
        isCycleDependent=  [false]
        isReadable=        [true]
        isMonitorable=     [true]
        isTransactional=   [false]
        isWritable=        [false]
        isValueMutable=    [false]
        Extra charact.=   
          DATA_SOURCE= [CCDB]
          IS_CCDB_DESCRIPTOR= [true]
          IS_RESTORABLE= [false]
          IS_SAVABLE= [false]
          IS_STATUS_PROPERTY= [false]
          IS_VALID= [true]
          PARAMETER_URL_GET= [rda3://rda3/CB.BHB1100/Acquisition#current_status]
          PARAMETER_URL_SET= [rda3://rda3/CB.BHB1100/Acquisition#current_status]
        
      acqStamps --> Parameter descriptor for CB.BHB1100/Acquisition#acqStamps
        Type               [Simple]
        Description=       [-]
        isCycleDependent=  [false]
        isReadable=        [true]
        isMonitorable=     [true]
        isTransactional=   [false]
        isWritable=        [false]
        isValueMutable=    [false]
        Extra charact.=   
          DATA_SOURCE= [CCDB]
          IS_CCDB_DESCRIPTOR= [true]
          IS_RESTORABLE= [false]
          IS_SAVABLE= [false]
          IS_STATUS_PROPERTY= [false]
          IS_VALID= [true]
          PARAMETER_URL_GET= [rda3://rda3/CB.BHB1100/Acquisition#acqStamps]
          PARAMETER_URL_SET= [rda3://rda3/CB.BHB1100/Acquisition#acqStamps]
        
      cycleName --> Parameter descriptor for CB.BHB1100/Acquisition#cycleName
        Type               [Simple]
        Description=       [-]
        isCycleDependent=  [false]
        isReadable=        [true]
        isMonitorable=     [true]
        isTransactional=   [false]
        isWritable=        [false]
        isValueMutable=    [false]
        Extra charact.=   
          DATA_SOURCE= [CCDB]
          IS_CCDB_DESCRIPTOR= [true]
          IS_RESTORABLE= [false]
          IS_SAVABLE= [false]
          IS_STATUS_PROPERTY= [false]
          IS_VALID= [true]
          PARAMETER_URL_GET= [rda3://rda3/CB.BHB1100/Acquisition#cycleName]
          PARAMETER_URL_SET= [rda3://rda3/CB.BHB1100/Acquisition#cycleName]
        
      currentAverage --> Parameter descriptor for CB.BHB1100/Acquisition#currentAverage
        Type               [Simple]
        Description=       [-]
        isCycleDependent=  [false]
        isReadable=        [true]
        isMonitorable=     [true]
        isTransactional=   [false]
        isWritable=        [false]
        isValueMutable=    [false]
        Extra charact.=   
          DATA_SOURCE= [CCDB]
          IS_CCDB_DESCRIPTOR= [true]
          IS_RESTORABLE= [false]
          IS_SAVABLE= [true]
          IS_STATUS_PROPERTY= [false]
          IS_VALID= [true]
          PARAMETER_URL_GET= [rda3://rda3/CB.BHB1100/Acquisition#currentAverage]
          PARAMETER_URL_SET= [rda3://rda3/CB.BHB1100/Acquisition#currentAverage]
        
    ]
    


Let's try a (simulated) SET
---------------------------

.. code:: python

    japc.setParam("CB.BHB1100/Acquisition#currentAverage", 4711)

An array can only be set as a whole!

.. code:: python

    setData = np.ones(3564)
    japc.setParam("LHC.BQS.SCTL/BunchSelector#BunchSel1Slots", setData)

Providing not enough or too many values results in an error...

.. code:: python

    setData = np.ones(3563)
    japc.setParam("LHC.BQS.SCTL/BunchSelector#BunchSel1Slots", setData)


::


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-18-e8a0c3856ce3> in <module>()
          1 setData = np.ones( 3563 )
    ----> 2 japc.setParam( "LHC.BQS.SCTL/BunchSelector#BunchSel1Slots", setData )
    

    site-packages/pyjapc/pyjapc.py in setParam(self, parameterName, parameterValue, **kwargs)
        490             if vdesc == None:
        491                 raise ValueError("Could not get a valueDescriptor. Can not do array dimension checks. Please initialize INCA in the PyJapc() constructor.")
    --> 492             parValNew = self._convertPyToVal( parameterValue, vdesc )     # Convert Python type to JAPC type
        493         #--------------------------------------------
        494         # Carry out the actual set (if not in safemode)


    site-packages/pyjapc/pyjapc.py in _convertPyToVal(self, pyVal, vdesc)
        520         # Also check array dimensions in Python
        521             if vdesc.type.typeString == "Simple":                         # Can be MAP or SIMPLE
    --> 522                 self._checkDimVsJAPC( pyVal, vdesc )                      # Check input array shape against FESA
        523                 parValNew = self._convertPyToSimpleVal( pyVal, vdesc )
        524             elif vdesc.type.typeString == "Map":


    site-packages/pyjapc/pyjapc.py in _checkDimVsJAPC(self, npArray, simpleValueDesc)
        707                 return
        708             else:
    --> 709                 raise TypeError( "Array dimensions do not agree for {0}. Please provide a 1D array of length {1}.".format( simpleValueDesc.name, l) )
        710         if rc == 0:
        711             rc = 1


    TypeError: Array dimensions do not agree for LHC.BQS.SCTL/BunchSelector#BunchSel1Slots. Please provide a 1D array of length 3564.


Doing a full SET
----------------

Note that the `syntax of a
parameter-name <https://confluence.cern.ch/display/JAPC/Parameter+Names>`__
is the following:

``[protocol://[service]/]</font>device/property[#field]``

If no field is specified (no ``#``-tag), several or all fields of a
property-group can be SET in one operation. This is important as some
FESA classes do not allow "partial SETs" and expect all fields beeing
SET in one go.

Setting several parameters at once can be done with python dictionarys
like this:

.. code:: python

    pprint.pprint(japc.getParam("CB.BHB1100/Acquisition"))


.. parsed-literal::

    {'acqStamps': array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0]),
     'current': array([ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
            0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
            0.,  0.,  0.,  0.,  0.,  0.]),
     'currentAverage': 0.0,
     'current_status': 0,
     'current_unit': 'A'}


.. code:: python

    myFields = {'currentAverage': 4.2, 'current_status': 42}
    japc.setParam("CB.BHB1100/Acquisition", myFields)

Subscribing to a parameter
--------------------------

First we need to define a callback function, which is called by JAPC
everytime new data is available.

In the callback we can do useful things, like updating a plot or writing
the new data to a file. For now we just print to the console.

.. code:: python

    def myCallback(parameterName, newValue):
        print(f"New value for {parameterName} is: {newValue}")

Then we need to register the callback function.

Note that a SubscriptionHandle is returned, which can be used to start
and stop the subscription to individual parameters.

.. code:: python

    japc.subscribeParam("CB.BHB1100/Acquisition#currentAverage", myCallback)


.. parsed-literal::

    <jpype._jclass.cern.japc.spi.adaptation.ViewParameter$SubscriptionHandleAdapter at 0x7f4ecb3a30b8>



Subscriptions can be started and stopped globally like this:

.. code:: python

    japc.startSubscriptions()


.. parsed-literal::

    New value for CB.BHB1100/Acquisition#currentAverage is: 0.0
    New value for CB.BHB1100/Acquisition#currentAverage is: 0.0
    New value for CB.BHB1100/Acquisition#currentAverage is: 0.0
    New value for CB.BHB1100/Acquisition#currentAverage is: 0.0
    New value for CB.BHB1100/Acquisition#currentAverage is: 0.0
    New value for CB.BHB1100/Acquisition#currentAverage is: 0.0
    New value for CB.BHB1100/Acquisition#currentAverage is: 0.0
    New value for CB.BHB1100/Acquisition#currentAverage is: 0.0


.. code:: python

    japc.stopSubscriptions()


.. parsed-literal::

    New value for CB.BHB1100/Acquisition#currentAverage is: 0.0


Note that subscriptions are retained even after stopping. If you want to
re-subscribe to different parameters you must clear the subscriptions
first.

.. code:: python

    japc.clearSubscriptions()
