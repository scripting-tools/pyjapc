.. _API_docs:

PyJapc API
==========

.. rubric:: Modules

.. autosummary::
   :toctree: api_docs

    pyjapc
    pyjapc.rbac_dialog

