import jpype as jp
import numpy as np
import pytest

import pyjapc


@pytest.mark.parametrize("shape, dtype", [
    ((10000, 10000), np.int32),
    ((10000, 10000), np.float32),
    ((10000, 10000), np.float64),
    ((1, 1), np.int32),
    ((1, 1), np.float32),
    ((1, 1), np.float64),
    ((1_000_000, ), np.int32),
    ((1_000_000, ), np.float32),
    ((1_000_000, ), np.float64),
])
def test_big_array(benchmark, shape, dtype):
    japc = pyjapc.PyJapc()
    data = np.linspace(0, 1000, np.prod(shape)).astype(dtype)

    DoubleArrayValue = jp.JClass("cern.japc.value.spi.value.simple.FloatArrayValue")
    v = DoubleArrayValue(jp.JArray(jp.JFloat)(data))

    benchmark(japc._convertSimpleValToPy, v)
