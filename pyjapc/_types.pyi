import typing

import cern
import java

import datetime

import sys

if sys.version_info > (3, 7):
    from typing import Protocol
else:
    # For Python 3.7, allow use of Protocol. When py37 dropped, move to typing.Protocol form.
    from typing_extensions import Protocol

# Input types
JString = typing.Union[java.lang.String, str]

DataFilter = typing.Dict[str, typing.Any]
Selector = str
Timezone = typing.Union[str, datetime.tzinfo]

LogLevelNames = typing.Union[
    typing.Literal['CRITICAL'],
    typing.Literal['FATAL'],
    typing.Literal['ERROR'],
    typing.Literal['WARN'],
    typing.Literal['WARNING'],
    typing.Literal['INFO'],
    typing.Literal['DEBUG'],
    typing.Literal['NOTSET'],
]

LogLevel = typing.Union[int, LogLevelNames, str]

# Note that we don't use Sequence[str] here as this includes str itself.
SequenceOfStrings = typing.Union[
    typing.List[str],
    typing.Tuple[str, ...],
]

# Common PyJapc (Python) types
HeaderType = typing.Dict[str, typing.Any]  # TODO: Could be TypedDict in the future.

# Common JAPC (Java) types
SubscriptionTypes = typing.Union[
    cern.japc.core.SubscriptionHandle,
    cern.japc.core.group.GroupSubscriptionHandle,
]
ParameterTypes = typing.Union[
    cern.japc.core.transaction.TransactionalParameter,
    cern.japc.core.group.ParameterGroup,
]

class PyRbacTokenLike(Protocol):
    def encode(self) -> bytes: ...
