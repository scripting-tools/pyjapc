"""The main pyjapc module which provides the :class:`PyJapc` class.

"""
import warnings

from ._japc import PyJapc
from ._version import version as __version__

__all__ = [
    'PyJapc',
    '__version__',
]

try:
    from .rbac_dialog import PasswordEntryDialogue, getPw
    __all__ += [
        'PasswordEntryDialogue',
        'getPw',
    ]
except ImportError:
    warnings.warn('Unable to import rbac_dialog Tkinter tools.')

__cmmnbuild_deps__ = [
    {"product": "accsoft-commons-domain", "groupId": "cern.accsoft.commons"},
    {"product": "japc", "groupId": "cern.japc"},
    {"product": "japc-ext-cmwrda3", "groupId": "cern.japc"},  # (indirect use) For RDA communication
    {"product": "japc-ext-inca", "groupId": "cern.japc"},
    {"product": "japc-ext-mockito2", "groupId": "cern.japc"},
    {"product": "japc-ext-remote", "groupId": "cern.japc"},  # (indirect use)
    {"product": "japc-svc-timing", "groupId": "cern.japc"},
    {"product": "japc-svc-ccs", "groupId": "cern.japc"},  # (indirect use) To fetch descriptors without INCA
    {"product": "japc-value", "groupId": "cern.japc"},
    {"product": "log4j", "groupId": "log4j"},
    {"product": "rbac-client", "groupId": "cern.accsoft.security"},
    {"product": "rbac-util", "groupId": "cern.accsoft.security"},
    {"product": "slf4j-api", "groupId": "org.slf4j"},
    {"product": "slf4j-log4j12", "groupId": "org.slf4j"},
]


# Make sure that the PyJapc class is seen to be coming from the top level
# module, rather than the private "_japc" one.
PyJapc.__module__ = __name__
